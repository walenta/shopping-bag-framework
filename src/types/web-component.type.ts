export class SbComponent extends HTMLElement {
  static formAssociated = true;

  internals: ElementInternals;
  shadowRoot: ShadowRoot;

  constructor() {
    super();
    this.internals = this.attachInternals();
    this.shadowRoot = this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    (window as any).Alpine.initTree(this.shadowRoot);
  }
}