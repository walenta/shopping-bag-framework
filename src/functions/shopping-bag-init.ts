import { SbWindow } from '../public-interfaces/sb-window.interface.js';
import { APP_COMPONENT, SB_COMPONENT_ATTR } from '../types/constants.js';
import { SbComponent } from '../types/web-component.type.js';
import Alpine from 'alpinejs';

declare const window: SbWindow;
window.sbComponents = {};
window.Alpine = Alpine;

const APP_COMPONENT_SELECTOR = `[${SB_COMPONENT_ATTR}='${APP_COMPONENT}']`;
const componentList = getComponentList();
initComponents(componentList);

function getComponentList() {
  return new Set(Array.from(
    document.querySelectorAll(`[${SB_COMPONENT_ATTR}]`),
    el => el.getAttribute(SB_COMPONENT_ATTR)
  ).filter(component => !!component.length && component !== APP_COMPONENT))
}

function initComponents(componentList: Set<string>) {
  const appElements = document.querySelectorAll(APP_COMPONENT_SELECTOR);
  if (!appElements?.length || appElements.length > 1) {
    throw new Error(`There must be only one App component. Found ${appElements.length} App components.`);
  }

  // @ts-ignore
  const imports = Array.from(componentList, componentName => import(/* webpackIgnore: true */ `./${componentName}.js`));
  Promise.all(imports)
    .then(async () => {
      for (const component in window.sbComponents) {
        const {componentClass} = window.sbComponents[component]
        new componentClass();
        customElements.define(sbComponentElementName(component), sbComponentElementConstructor());
      }

      document.addEventListener('alpine:init', () => {
        initAlpineData();
        initAlpineComponents();
      }, {once: true});

      // @ts-ignore
      await import(/* webpackIgnore: true */ './main.js');
    })
    .catch(err => {
      throw new Error('Error importing components.', err);
    });
}

function initAlpineData() {
  for (const component in window.sbComponents) {
    const {alpineObject} = window.sbComponents[component];
    if (!alpineObject) {
      throw new Error(`Alpine object of ${component} component is not defined`);
    }

    window[alpineObjectKey(component)] = alpineObject;
  }
}

function initAlpineComponents(rootElement = document.querySelector(APP_COMPONENT_SELECTOR), componentElements = Array.from(document.querySelectorAll(`[${SB_COMPONENT_ATTR}]`))) {
  const children = getChildComponents(rootElement, componentElements);
  children.forEach(component => {
    const componentName = component.getAttribute(SB_COMPONENT_ATTR);
    const outerHTML = component.outerHTML;

    const componentEl = document.createElement(sbComponentElementName(componentName));
    componentEl.shadowRoot.innerHTML = outerHTML;

    const sbComponentRoot = componentEl.shadowRoot.querySelector(`[${SB_COMPONENT_ATTR}]`);
    sbComponentRoot.setAttribute('x-data', alpineObjectKey(componentName));
    sbComponentRoot.setAttribute('x-init', 'onInit($root, $nextTick)');
    
    window.sbComponents[componentName].componentStyles.forEach(style => {
      style.use({ target: componentEl.shadowRoot });
    })

    const componentParent = component.parentElement;
    component.remove();
    setTimeout(() => {
      componentParent.appendChild(componentEl)
    });

    initAlpineComponents(sbComponentRoot, Array.from(componentEl.shadowRoot.querySelectorAll(`[${SB_COMPONENT_ATTR}]`)))
  });
}

function getChildComponents(element: Element, componentElements: Element[]) {
  return componentElements.filter(componentEl => !componentEl.isEqualNode(element) && componentEl.parentElement.closest(`[${SB_COMPONENT_ATTR}]`).isEqualNode(element));
}

const sbComponentElementName = (sbComponentName: string) => `x-${sbComponentName}`;

const alpineObjectKey = (sbComponentName: string) => `sb_alpine_${sbComponentName}`;

const sbComponentElementConstructor = () => class extends SbComponent {};