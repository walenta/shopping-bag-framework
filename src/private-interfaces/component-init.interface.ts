import Init from "../public-interfaces/init.interface";
import { NextTickFn } from "../types/next-tick.type";

export default interface ComponentInit extends Init {
  onInit(rootElement?: HTMLElement, nextTick?: NextTickFn): void;
  
}