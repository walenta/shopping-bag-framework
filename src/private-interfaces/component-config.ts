export interface ComponentConfig {
  selector: string;
  importedStyles: unknown[];
}