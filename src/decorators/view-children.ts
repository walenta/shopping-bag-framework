export default function ViewChildren(cssSelector: string) {
  return function (target: any, key: string | symbol) {
    if (!target.afterInitCallbacks) {
      Object.defineProperty(target, 'afterInitCallbacks', {
        value: [],
        writable: false
      })
    }
    target.afterInitCallbacks.push(() => {
      const root = (target as any).componentRoot;
      const value = root.querySelectorAll(cssSelector);
      Object.defineProperty(target, key, {
        value,
        enumerable: true,
      });
    });
  }
}