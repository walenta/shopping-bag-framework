export default function ShopifyData(apiEndpoint: string, responseAsJson = true) {
  return function (target: Object, key: string | symbol) {
    const url = `${(window as any).Shopify.routes.root}${apiEndpoint}${!apiEndpoint.includes('.js') ? '.js' : ''}`;
    const value = fetch(url).then(res => responseAsJson ? res.json() : res);
    Object.defineProperty(target, key, {
      value,
      enumerable: true,
    })
  }
}