import { ComponentConfig } from "../private-interfaces/component-config";
import ComponentInit from "../private-interfaces/component-init.interface";
import { SbComponentObject } from "../public-interfaces/sb-component-object.interface";
import { SbWindow } from "../public-interfaces/sb-window.interface";
import { NextTickFn } from "../types/next-tick.type";

declare const window: SbWindow;

export default function Component(config: ComponentConfig) {
  return function <T extends { new(...args: any[]): {} }>(constructor: T) {
    class ComponentClass extends constructor implements ComponentInit {
      constructor(...args: any[]) {
        super(...args);

        this.#constructAlpineObject();
      }

      #constructAlpineObject() {
        const alpineObj = {} as any;
        
        for (const prop in this) {
          alpineObj[prop] = this[prop];
        }

        Object.getOwnPropertyNames(constructor.prototype).forEach(method => {
          if (method !== 'constructor') {
            alpineObj[method] = constructor.prototype[method];
          }
        })
        
        Object.getOwnPropertyNames(ComponentClass.prototype).forEach(method => {
          if (method !== 'constructor') {
            alpineObj[method] = ComponentClass.prototype[method];
          }
        });

        window.sbComponents[config.selector].alpineObject = alpineObj;
      }

      onInit(root: HTMLElement, nextTick: NextTickFn): void {
        constructor.prototype.componentRoot = root;
        if (constructor.prototype?.onInit) {
          constructor.prototype.onInit(root);
        }

        nextTick(() => {
          if (constructor.prototype?.afterInit) {
            constructor.prototype.afterInitCallbacks.forEach((cb: () => void) => {
              cb();
            });
            constructor.prototype.afterInit();
          }
        }); 
      }
    };

    window.sbComponents[config.selector] = {} as SbComponentObject;
    window.sbComponents[config.selector].componentClass = ComponentClass;
    window.sbComponents[config.selector].componentStyles = config.importedStyles;

    
    return ComponentClass;
  }
}