export default interface Init {
  onInit(): void;
  init?: never;
}