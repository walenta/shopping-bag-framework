import { Alpine } from "alpinejs";
import { SbComponents } from "./sb-component-object.interface";

export interface SbWindow extends Window {
  sbComponents: SbComponents;
  Alpine: Alpine
  [key: string]: any;
}