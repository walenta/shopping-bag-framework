export interface SbComponents {
  [component: string]: SbComponentObject;
}

export interface SbComponentObject {
  alpineObject: any;
  componentClass: any;
  componentStyles: any[];
}