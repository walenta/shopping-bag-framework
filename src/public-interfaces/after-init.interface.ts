export default interface AfterInit {
  afterInit(): void;
}